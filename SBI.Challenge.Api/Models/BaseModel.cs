﻿using System;
using System.ComponentModel.DataAnnotations;
using SBI.Challenge.Api.Attributes;
using Newtonsoft.Json;

namespace SBI.Challenge.Api.Models
{
    public class BaseModel
    {
        [StringLength(64)]
        public string Id { get; set; } = Guid.NewGuid().ToString();

        [JsonConverter(typeof(DateFormatConverter), "yyyy-MM-ddTHH:mm:ss.fffZ")]
        public DateTime? RegisterDate { get; set; }

        [StringLength(128)]
        public string RegisterBy { get; set; }

        [JsonConverter(typeof(DateFormatConverter), "yyyy-MM-ddTHH:mm:ss.fffZ")]
        public DateTime? UpdatedDate { get; set; }

        [StringLength(128)]
        public string UpdatedBy { get; set; }

        [JsonConverter(typeof(DateFormatConverter), "yyyy-MM-ddTHH:mm:ss.fffZ")]
        public DateTime? DeletedDate { get; set; }

        [StringLength(128)]
        public string DeletedBy { get; set; }

        public bool IsDeleted { get; set; } = false;
    }
}
