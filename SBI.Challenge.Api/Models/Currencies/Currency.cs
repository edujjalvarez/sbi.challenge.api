﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBI.Challenge.Api.Models.Currencies
{
    [Table("Currencies")]
    public class Currency : BaseModel
    {
        [Required]
        [StringLength(64)]
        public string Name { get; set; }
    }
}
