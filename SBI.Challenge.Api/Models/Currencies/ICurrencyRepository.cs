﻿namespace SBI.Challenge.Api.Models.Currencies
{
    public interface ICurrencyRepository : IBaseRepository<Currency>
    {
    }
}
