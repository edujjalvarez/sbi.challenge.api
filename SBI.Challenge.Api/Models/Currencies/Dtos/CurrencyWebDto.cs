﻿using System.ComponentModel.DataAnnotations;

namespace SBI.Challenge.Api.Models.Currencies.Dtos
{
    public class CurrencyWebDto : BaseDto
    {
        [Required]
        [StringLength(64)]
        public string Name { get; set; }
    }
}
