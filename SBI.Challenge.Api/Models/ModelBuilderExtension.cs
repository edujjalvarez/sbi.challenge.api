﻿using Microsoft.EntityFrameworkCore;
using SBI.Challenge.Api.Migrations.Seed;

namespace SBI.Challenge.Api.Models
{
    public static class ModelBuilderExtension
    {
        public static void ApplyConfigurations(this ModelBuilder modelBuilder)
        {
            //modelBuilder.ApplyConfiguration(new UserConfiguration());
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.SeedCurrencies();
            modelBuilder.SeedQuotes();
        }
    }
}
