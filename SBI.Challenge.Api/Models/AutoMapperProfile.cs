﻿using AutoMapper;
using SBI.Challenge.Api.Models.ServerPosts;

namespace SBI.Challenge.Api.Models
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ServerPost, Salida>()
                .ForMember(dest => dest.Id, act => act.MapFrom(src => src.id))
                .ForMember(dest => dest.Titulo, act => act.MapFrom(src => src.title));
        }
    }
}
