﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SBI.Challenge.Api.Models
{
    public class BaseDto
    {
        [StringLength(64)]
        public string Id { get; set; } = Guid.NewGuid().ToString();
    }
}
