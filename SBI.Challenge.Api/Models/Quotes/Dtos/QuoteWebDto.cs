﻿using System.ComponentModel.DataAnnotations;

namespace SBI.Challenge.Api.Models.Quotes.Dtos
{
    public class QuoteWebDto : BaseDto
    {
        [Required]
        [StringLength(4)]
        public string Source { get; set; }

        public double Value { get; set; }
    }
}
