﻿namespace SBI.Challenge.Api.Models.Quotes
{
    public interface IQuoteRepository : IBaseRepository<Quote>
    {
    }
}
