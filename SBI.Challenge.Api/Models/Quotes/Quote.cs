﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBI.Challenge.Api.Models.Quotes
{
    [Table("Quotes")]
    public class Quote : BaseModel
    {
        [Required]
        [StringLength(4)]
        public string Source { get; set; }

        public double Value { get; set; }
    }
}
