﻿using AutoMapper;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace SBI.Challenge.Api.Models.ServerPosts
{
    public class GetSalidaByIdRequestHandler : IRequestHandler<GetSalidaByIdRequest, Salida>
    {
        private readonly HttpClient _httpClient;
        private readonly IMapper _mapper;

        public GetSalidaByIdRequestHandler(IMapper mapper)
        {
            _httpClient = new HttpClient();
            _mapper = mapper;
        }

        public async Task<Salida> Handle(GetSalidaByIdRequest request, CancellationToken cancellationToken)
        {
            var endpoint = "https://jsonplaceholder.typicode.com/posts";
            var uri = new Uri(endpoint);
            var response = await _httpClient.GetAsync(uri, cancellationToken);
            if (!response.IsSuccessStatusCode) return null;
            var responseStr = response.Content.ReadAsStringAsync().Result;
            var serverPosts = JsonConvert.DeserializeObject<List<ServerPost>>(responseStr);
            var serverPost = serverPosts.SingleOrDefault(sp => sp.id.Equals(request.id));
            var salida = _mapper.Map<Salida>(serverPost);
            return salida;
        }
    }
}
