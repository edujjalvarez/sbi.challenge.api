﻿namespace SBI.Challenge.Api.Models.ServerPosts
{
    public class Salida
    {
        public int Id { get; set; }

        public string Titulo { get; set; }
    }
}
