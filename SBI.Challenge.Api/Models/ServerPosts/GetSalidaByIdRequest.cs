﻿using MediatR;

namespace SBI.Challenge.Api.Models.ServerPosts
{
    public class GetSalidaByIdRequest : IRequest<Salida>
    {
        public int id { get; set; }
    }
}
