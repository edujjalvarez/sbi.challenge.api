﻿using SBI.Challenge.Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using SBI.Challenge.Api.Models.Currencies;
using SBI.Challenge.Api.Models.Quotes;
using SBI.Challenge.Api.Services.CurrencyConversions;
using AutoMapper;

namespace SBI.Challenge.Api
{
    public static class DependencyInjectionExtension
    {
        public static void InjectDependencies(this IServiceCollection services)
        {
            // Repositories
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped(typeof(ICurrencyRepository), typeof(CurrencyRepository));
            services.AddScoped(typeof(IQuoteRepository), typeof(QuoteRepository));

            // Scoped Services
            services.AddScoped<ICurrencyConversionService, CurrencyConversionService>();

            // Transient Services
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
        }

        public static void ConfigureAutoMapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });
            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
