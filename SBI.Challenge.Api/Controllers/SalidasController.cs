﻿using Microsoft.AspNetCore.Mvc;
using MediatR;
using SBI.Challenge.Api.Models.ServerPosts;
using System.Threading.Tasks;

namespace SBI.Challenge.Api.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/salidas")]
    public class SalidasController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SalidasController(
            IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await _mediator.Send(new GetSalidaByIdRequest
            {
                id = id
            });
            return Ok(result);
        }
    }
}
